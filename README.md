# paren-particles: TechBookFest 7 circle cut

It's our circle cut for TechBookFest7 written in Clojure.

![paren-particles](paren-particles.gif)

## Usage

LightTable - open `core.clj` and press `Ctrl+Shift+Enter` to evaluate the file.

Emacs - run cider, open `core.clj` and press `C-c C-k` to evaluate the file.

REPL - run `(require 'techbookfest7-circle-cut.core)`.

## Author

- paren-holic

## License

Distributed under the Eclipse Public License either version 1.0 or (at
your option) any later version.
