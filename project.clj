(defproject techbookfest7-circlecut "0.1.0-SNAPSHOT"
  :description "paren-particles: paren-holic's circle cut for TechBookFest7"
  :url "http://example.com/FIXME"
  :license {:name "Eclipse Public License"
            :url "http://www.eclipse.org/legal/epl-v10.html"}
  :dependencies [[org.clojure/clojure "1.9.0"]
                 [quil "3.0.0"]])
